resource "aws_security_group" "ssh" {
  name        = "allow_ssh"
  description = "Permitir acesso remoto via porta 22(ssh)"

  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_vpc_security_group_egress_rule" "allow_ssh_ipv4" {
  security_group_id = aws_security_group.ssh.id
  cidr_ipv4         = "0.0.0.0/0"  # Substitua isso pelo CIDR apropriado, se necessário
  from_port         = 22
  ip_protocol       = "tcp"
  to_port           = 22
}

resource "aws_security_group" "http" {
  name        = "allow_http"
  description = "Permitir acesso remoto via porta 80(http)"

  tags = {
    Name = "allow_http"
  }
}

resource "aws_vpc_security_group_egress_rule" "allow_http_ipv4" {
  security_group_id = aws_security_group.http.id
  cidr_ipv4         = "0.0.0.0/0"  # Substitua isso pelo CIDR apropriado, se necessário
  from_port         = 80
  ip_protocol       = "tcp"
  to_port           = 80
}

resource "aws_vpc_security_group_egress_rule" "allow_all_traffic_ipv4" {
  security_group_id = aws_security_group.http.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = "-1" # semantically equivalent to all ports
}

resource "aws_security_group" "egress" {
  name        = "allow_egress"
  description = "Permitir egress"

  tags = {
    Name = "allow_egress"
  }
  
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
